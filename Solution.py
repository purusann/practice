# # Problem_1
#
# input_string = input("Input string to be encrypted: ")
# input_step = int(input("Input step: "))
#
# # To store the ascii value
# ascii_value = []
# # To store the character
# char_value = ""
#
# for char in input_string:
#     # ascii_value.append(ord(i))
#     ascii_value.append(int(ord(char)))
#
# # To delete the space
# while 32 in ascii_value:
#     ascii_value.remove(32)
#
# # To reconvert
# restart_with_upper_case = 64
# restart_with_lower_case = 96
#
# for i in ascii_value:
#     if i in range(65, 90+1):
#         i += input_step
#         if i > 90:
#             if i == 91:
#                 i = restart_with_upper_case+1
#             elif i == 92:
#                 i = restart_with_upper_case+2
#             elif i == 93:
#                 i = restart_with_upper_case+3
#             elif i == 94:
#                 i = restart_with_upper_case+4
#             elif i == 95:
#                 i = restart_with_upper_case+5
#
#     elif i in range(97, 122+1):
#         i += input_step
#         if i > 122:
#             if i == 123:
#                 i = restart_with_lower_case+1
#             elif i == 124:
#                 i = restart_with_lower_case+2
#             elif i == 125:
#                 i = restart_with_lower_case+3
#             elif i == 126:
#                 i = restart_with_lower_case+4
#             elif i == 127:
#                 i = restart_with_lower_case+5
#     char_value = char_value + chr(i)
#
# print(str(char_value))
# # ABCDEFGHIJKLMNOPQRSTUVWXYZ
# # abcdefghijklmnopqrstuvwxyz
# # Hello WorldXyZ

# Problem_2
input_string = input("Input String: ")

if 'poor' or 'poor!' in input_string:
    add_good = input_string.replace('poor', 'good')
    if 'not' or 'not that' or 'not so' or 'not also' or 'no' in input_string:
        remove_not = add_good.replace('not', '')
        remove_that = remove_not.replace('that', '')
        remove_also = remove_that.replace('also', '')
        remove_no = remove_also.replace('no', '')
        removed = remove_no.replace('so', '')
        print(removed)

# elif 'not' or 'not that' or 'not so' in input_string:
#     input_string.replace('not', ' ')
#     input_string.replace('so', '')
#     input_string.replace('that', '')
#     # remove_not = input_string.replace('not', ' ')
#     # remove_that = remove_not.replace('that', '')
#     # remove_so = remove_that.replace('so', '')
#     if 'poor' or 'poor!' in input_string:
#         add_good = input_string.replace('poor', 'good')
#         print(add_good)
#
#     if 'poor' or 'poor!' in input_string:
#         add_good = remove_no.replace('poor', 'good')
#         print(add_good)


# This lyrics is poor!
# This lyrics is not that poor!
# This lyrics is no poor, the melody is not poor!
# This lyrics is not that poor, the melody is not so poor!

